;;; m-lsp.el -- language server protocol -*- lexical-binding: t; -*-

;;; Commentary:

;; Setup lsp packages.

;;; Code:

(use-package flycheck
  :diminish flycheck-mode
  :hook
  (after-init . global-flycheck-mode)
  :config
  (when (featurep 'evil)
    (evil-define-key 'normal 'global "gl" #'flycheck-display-error-at-point)))

;; Show in posframe
(use-package flycheck-posframe
  :after flycheck
  :hook (flycheck-mode . flycheck-posframe-mode))

;; Yaml linter with flycheck
(use-package flycheck-yamllint
  :if (executable-find "yamllint")
  :hook ((yaml-mode . flycheck-yamllint-setup)
         (yaml-mode . flycheck-mode)))

(use-package yasnippet
  :diminish yas-minor-mode
  :hook
  (after-init . yas-global-mode)
  (org-mode . yas-global-mode)
  :commands yas-insert-snippet
  :config
  (yas-reload-all))

;; Another LSP implementation
(use-package eglot
  :elpaca nil
  :commands eglot
  :config
  (add-to-list 'eglot-server-programs
               '(c-mode . ("clangd")))
  (add-to-list 'eglot-server-programs
               '(rust-mode . ("rust-analyzer")))
  (add-to-list 'eglot-server-programs
               '(v-mode . ("vls")))
  (add-to-list 'eglot-server-programs
               '(zig-mode . ("zls")))
  :custom
  ;; Shutdown server when last managed buffer is killed
  (eglot-autoshutdown t))

(provide 'm-lsp)
;;; m-lsp.el ends here
