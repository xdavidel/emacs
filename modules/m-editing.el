;;; m-editing.el -- editing related stuff -*- lexical-binding: t; -*-

;;; Commentary:

;; Editing text configuration.

;;; Code:

;; Be smart when using parens, and highlight content
(if (fboundp 'electric-pair-mode)
    (electric-pair-mode 1)) ; auto-insert matching bracket

;; Show matching parens
(use-package paren
  :elpaca nil
  :init
  (setq show-paren-delay 0)
  (show-paren-mode t))

;; whitespace
(use-package whitespace
  :elpaca nil
  :diminish whitespace-mode
  :custom
  (whitespace-style '(face tabs empty trailing tab-mark indentation::space))
  (whitespace-action '(cleanup auto-cleanup))
  :hook
  (prog-mode . whitespace-mode)
  (text-mode . whitespace-mode))

;; Move lines around easily
(use-package drag-stuff
  :diminish drag-stuff-mode
  :init
  (drag-stuff-global-mode 1)
  :general
  (drag-stuff-mode-map
   "M-<up>"   'drag-stuff-up
   "M-<down>" 'drag-stuff-down))

;; Set color backgrounds to color names
(use-package rainbow-mode
  :diminish rainbow-mode
  :hook (prog-mode . rainbow-mode))

;; Rainbow colors for brackets
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;; Format collection for every mode
(use-package format-all)

;; Move easily in shown region
(use-package avy
  :general
  ("M-s" 'avy-goto-char-timer)
  ('(normal visual) :prefix "<leader>"
   "SPC SPC" '("goto line"      . avy-goto-line)
   "SPC j"   '("jump line down" . avy-goto-line-below)
   "SPC k"   '("jump line up"   . avy-goto-line-above)))

(provide 'm-editing)
;;; m-editing.el ends here
