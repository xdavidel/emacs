;;; m-screencast.el -- screen cast -*- lexical-binding: t; -*-

;;; Commentary:
;; Screencast configuration

;;; Code:

;; Display key presses
(use-package keycast
  :commands (keycast-mode)
  :custom
  (keycast-remove-tail-elements nil)
  (keycast-insert-after 'mode-line-misc-info))

(provide 'm-screencast)
;;; m-screencast.el ends here
