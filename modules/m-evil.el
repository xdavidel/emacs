;;; m-evil.el --vi-mode -*- lexical-binding: t; -*-

;;; Commentary:

;; Evil mode configuration, for a `vi' keybindings.

;;; Code:

;; Vim-like keybindings
(use-package evil
  :general
  (evil-insert-state-map
        "C-g" 'evil-normal-state
        "C-h" 'evil-delete-backward-char-and-join)
  (evil-window-map
        "<left>"  'evil-window-left
        "<up>"    'evil-window-up
        "<right>" 'evil-window-right
        "<down>"  'evil-window-down
        "C-g"     'evil-normal-state
        "C-h"     'evil-delete-backward-char-and-join)
  :custom
  (evil-want-integration t)
  (evil-want-keybinding nil)
  (evil-want-C-u-scroll t)
  (evil-want-C-i-jump nil)
  (evil-respect-visual-line-mode t)
  (evil-undo-system 'undo-redo)
  (evil-vsplit-window-right t)
  (evil-split-window-below t)
  :init
  (setq evil-want-Y-yank-to-eol t)
  (evil-mode 1)
  :config
  (evil-set-leader 'normal emacs-leader-key)
  ;; Rebind `universal-argument' to 'C-M-u' since 'C-u' now scrolls the buffer
  (global-set-key (kbd "C-M-u") 'universal-argument)

  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  ;; Remove those evil bindings
  (with-eval-after-load 'evil-maps
    (define-key evil-motion-state-map (kbd "SPC") nil)
    (define-key evil-motion-state-map (kbd "RET") nil)
    (define-key evil-motion-state-map (kbd "TAB") nil))

  ;; make evil-search-word look for symbol rather than word boundaries
  (with-eval-after-load 'evil
    (defalias #'forward-evil-word #'forward-evil-symbol)
    (setq-default evil-symbol-word-search t)))

;; Vim like surround package
(use-package evil-surround
  :init
  (global-evil-surround-mode))

(use-package evil-collection
  :diminish evil-collection-unimpaired-mode
  :after evil
  :init
  (evil-collection-init))

;; Comment code efficiently
(use-package evil-nerd-commenter
  :general
  (global-map
   "C-/" 'evilnc-comment-or-uncomment-lines
   [remap comment-dwim] 'evilnc-comment-or-uncomment-lines)
  :after evil
  :commands (evilnc-comment-or-uncomment-lines)
  :init
  ;; Set a global binding for better line commenting/uncommenting
  (general-def 'normal "gcc" 'evilnc-comment-or-uncomment-lines)
  (general-def 'visual "gc"  'evilnc-comment-or-uncomment-lines))

;; Increment / Decrement binary, octal, decimal and hex literals
(use-package evil-numbers
  :init
  (general-def '(normal visual) "C-a" 'evil-numbers/inc-at-pt)
  (general-def '(normal visual) "g C-a" 'evil-numbers/inc-at-pt-incremental)
  :commands (evil-numbers/inc-at-pt evil-numbers/inc-at-pt-incremental))

;;; Evil Bindings

;; Applications
(general-define-key (kbd "<leader>a") '("application" . (keymap)))

;; Buffers & windows
(general-define-key (kbd "<leader>b")  '("buffer"        . (keymap)))
(general-define-key (kbd "<leader>bs") '("switch buffer" . switch-to-buffer))
(general-define-key (kbd "<leader>bi") '("indent buffer" . indent-whole-buffer))
(general-define-key (kbd "<leader>be") '("show diff"     . ediff-buffers))

;; Config
(general-define-key (kbd "<leader>c")  '("config"               . (keymap)))
(general-define-key (kbd "<leader>cr") '("reload configuration" . reload-configuration))
(general-define-key (kbd "<leader>c.") '("open configuration"   . open-config-dir))

;; Files
(general-define-key (kbd "<leader>f")  '("file"          . (keymap)))
(general-define-key (kbd "<leader>ff") '("find file"     . find-file))

;; Git
(general-define-key (kbd "<leader>g")  '("git" . (keymap)))

;; Lsp
(general-define-key (kbd "<leader>l")  '("lsp" . (keymap)))

;; Org
(general-define-key (kbd "<leader>o")  '("org" . (keymap)))

;; Quiting
(general-define-key (kbd "<leader>q") '("quit" . (keymap)))
(general-define-key (kbd "<leader>qq") '("close buffer" . kill-buffer-and-window))

;; Search
(general-define-key (kbd "<leader>s")   '("search"           . (keymap)))
(general-define-key (kbd "<leader>si")  '("internet"         . (keymap)))

;; Window
(general-define-key (kbd "<leader>w")  '("window"          . (keymap)))
(general-define-key (kbd "<leader>ww") '("tear off window" . tear-off-window))
(general-define-key (kbd "<leader>wh") '("swap left"       . windmove-swap-states-left))
(general-define-key (kbd "<leader>wj") '("swap down"       . windmove-swap-states-down))
(general-define-key (kbd "<leader>wk") '("swap up"         . windmove-swap-states-up))
(general-define-key (kbd "<leader>wl") '("swap right"      . windmove-swap-states-right))

(general-def 'normal prog-mode-map
  "K" 'eldoc-print-current-symbol-info)

(general-def 'normal emacs-lisp-mode-map
  "K" 'describe-thing-at-point)

(general-def 'normal 'compilation-mode-map
  (kbd "C-n") 'compilation-next-error
  (kbd "C-p") 'compilation-previous-error)

(provide 'm-evil)
;;; m-evil.el ends here
