;;;; m-remote.el --- Configuration for remote development  -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

;; Manage remotes
(use-package tramp
  :elpaca nil
  :config
  ;; Set default connection mode to SSH
  (setq tramp-default-method "ssh"
        tramp-verbose 6))

;; Docker
(use-package docker
  :commands docker
  :general
  (global-map "C-c d" 'hydra-docker/body)
  ('normal :prefix "<leader>" "ad" '("docker"      . docker))
  :config
  (defhydra hydra-docker (:columns 5 :color blue)
    "Docker"
    ("c" docker-containers "Containers")
    ("v" docker-volumes "Volumes")
    ("i" docker-images "Images")
    ("n" docker-networks "Networks")
    ("b" dockerfile-build-buffer "Build Buffer")
    ("q" nil "Quit")))

(use-package tramp-container
  :elpaca nil
  :after docker)

(provide 'm-remote)
;;; m-remote.el ends here
