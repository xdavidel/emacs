;;; m-ui.el -- ui related configuration -*- lexical-binding: t; -*-

;;; Commentary:
;; User interface customizations.
;; Examples are the modeline and how help buffers are displayed.

;;; Code:

;; Emacs does not need pager
(setenv "PAGER" "cat")

(use-package all-the-icons
  :diminish
  :config
  (when (and (not (font-installed-p "all-the-icons"))
             (window-system))
    (all-the-icons-install-fonts t)))

;; Icons for dired
(use-package all-the-icons-dired
  :diminish
  :hook (dired-mode . all-the-icons-dired-mode))

;; Clear background on changing theme
(unless (display-graphic-p)
  (add-hook 'after-load-theme-hook #'clear-bg))

;; Line numbers
(use-package display-line-numbers
  :elpaca nil
  :hook
  (prog-mode . display-line-numbers-mode)
  :custom
  (display-line-numbers-width 4)
  (display-line-numbers-width-start t)
  (display-line-numbers-type 'relative))

(use-package column-number
  :elpaca nil
  :hook
  (prog-mode . column-number-mode))

;; Better help override bindings
(use-package helpful
  :general
  (global-map
   [remap describe-function] 'helpful-callable
   [remap describe-symbol]   'helpful-symbol
   [remap describe-variable] 'helpful-variable
   [remap describe-command]  'helpful-command
   [remap describe-key]      'helpful-key
   "C-h C-k"                 'helpful-at-point
   "C-h K"                   'describe-keymap
   "C-h F"                   'helpful-function)
  (helpful-mode-map
   [remap revert-buffer]     'helpful-update))

;; Set fonts if possible
(cond ((font-installed-p "Cascadia Code")
       (set-face-attribute 'default nil :font "Cascadia Code 10"))
      ((font-installed-p "JetBrainsMono")
       (set-face-attribute 'default nil :font "JetBrainsMono 10"))
      ((font-installed-p "Hack")
       (set-face-attribute 'default nil :font "Hack 10")))


;; add visual pulse when changing focus, like beacon but built-in
(use-package pulse
  :elpaca nil
  :unless (display-graphic-p)
  :init
  (defun pulse-line (&rest _)
    (pulse-momentary-highlight-one-line (point)))
  (dolist (cmd '(recenter-top-bottom
                 other-window windmove-do-window-select
                 pager-page-down pager-page-up
                 winum-select-window-by-number
                 ;; treemacs-select-window
                 symbol-overlay-basic-jump))
    (advice-add cmd :after #'pulse-line))) ; Highlight cursor postion after movement

;; Show eldoc information in popup
(use-package eldoc-box
  :diminish (eldoc-mode eldoc-box-hover-at-point-mode)
  :if (display-graphic-p)
  :config
  :hook (eldoc-mode . eldoc-box-hover-at-point-mode))


(use-package mouse
  :elpaca nil
  :init
  (context-menu-mode 1))


(provide 'm-ui)
;;; m-ui.el ends here
