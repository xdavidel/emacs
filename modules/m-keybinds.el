;;; m-keybinds.el -- centeral key bindings -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

;; Display keys in a menu
(use-package which-key
  :diminish which-key-mode
  :demand
  :init
  (setq which-key-idle-delay 0.5)
  :config
  ;; Shows available keybindings after you start typing.
  (which-key-mode 1))

;; Repeater keys
(use-package hydra)

(global-set-key (kbd "C-c C-.") #'open-config-dir)

;; Manpages
(global-set-key (kbd "C-c s m") #'man-or-woman)

;; Line Wrap
(global-set-key (kbd "M-z") #'toggle-line-wrap)

;; Change for size
(general-def "C-=" 'text-scale-increase)
(general-def "C--" 'text-scale-decrease)
(general-def "<C-mouse-4>" 'text-scale-increase)
(general-def "<C-mouse-5>" 'text-scale-decrease)

;; Makes <escape> quit as much as possible.
(general-def global-map "<escape>" 'keyboard-escape-quit)
(general-def minibuffer-local-map "<escape>" 'keyboard-escape-quit)
(general-def minibuffer-local-ns-map "<escape>" 'keyboard-escape-quit)
(general-def minibuffer-local-completion-map "<escape>" 'keyboard-escape-quit)
(general-def minibuffer-local-must-match-map "<escape>" 'keyboard-escape-quit)
(general-def minibuffer-local-isearch-map "<escape>" 'keyboard-escape-quit)

;; Easier eval expression bindings
(general-def "M-RET" 'eval-expression)

;; Close buffer without closing emacs
(general-def "C-c x" 'kill-this-buffer)

;; Toggle line wrap
(general-def "C-c x" 'toggle-line-wrap)

;; Expand macros in `emacs'
(general-def emacs-lisp-mode-map "C-x e" 'emacs-lisp-macroexpand)


(provide 'm-keybinds)
;;; m-keybinds.el ends here
