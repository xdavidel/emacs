;;; init.el --- Emacs main configuration file -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; Consts
(defconst IS-MAC          (eq system-type 'darwin)
  "Does Emacs runs on a macos system.")
(defconst IS-LINUX        (eq system-type 'gnu/linux)
  "Does Emacs runs on a linux based operating system.")
(defconst IS-WINDOWS      (memq system-type '(cygwin windows-nt ms-dos))
  "Does Emacs runs on a windows based system.")
(defconst IS-BSD          (or IS-MAC (eq system-type 'berkeley-unix))
  "Does Emacs runs on BSD based system.")

(defun termux-p ()
  "Check if Emacs is running under Termux."
  (string-match-p
   (regexp-quote "/com.termux/")
   (expand-file-name "~")))


(customize-set-variable 'custom-file
                        (expand-file-name "custom.el" user-emacs-directory-orig))

;; Add the modules folder to the load path
(add-to-list 'load-path (expand-file-name "modules/" user-emacs-directory-orig))

(defcustom emacs-leader-key " "
  "The key to use as a leader key.")


;; Load modules here
(require 'm-package-manager)

;; general for kybinding
(use-package general
  :demand
  :config
  (general-define-key "C-c ?" 'general-describe-keybindings))

(use-package diminish
  :demand)

(elpaca-wait)

(require 'm-helpers)
(require 'm-defaults)
(require 'm-screencast)
(require 'm-ui)
(require 'm-editing)
(require 'm-evil)
(require 'm-completion)
(require 'm-project)
(require 'm-org)
(require 'm-lsp)
(require 'm-keybinds)
(require 'm-tools)
(require 'm-shell)
(require 'm-remote)
(require 'm-modes)

;; ------------------------------------

;; Enable server
(use-package server
  :elpaca nil
  :config
  (or (server-running-p)
      (server-start)))

(provide 'init)
;;; init.el ends here
